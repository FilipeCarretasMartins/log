function log(a, b) {
    let finalArr = [];
    let initial = parseInt(a);
    let final = parseInt(b);
    let str;
    if (initial > final) {
        return 'Erro! O número inicial tem de ser inferior ao final!';
    } else if (!Number.isInteger(initial) || !Number.isInteger(final)) {
        return 'O(s) valor(es) introduzido(s) não é/são inteiro(s)!';
    } else {
        for (let i = initial; i <= final; i++) {
            str = "";
            if (i % 3 === 0) {
                str += 'fizz ';
            }
            if((i+'').indexOf('3') > -1){
                str += 'fizz ';
            }
            if (i % 5 === 0) {
                str += 'buzz ';
            }
            if((i+'').indexOf('5') > -1){
                str += 'buzz ';
            }
            if (i % 3 === 0 && i % 5 === 0) {
                str = 'fizzbuzz ';
            }
            if (((i % 3 === 0 || (i+'').indexOf('3') > -1) ||
                (i % 5 === 0 || (i+'').indexOf('5') > -1)) && (i % 3 === 0 && i % 5 === 0)) {
                str = 'lucky ';
            }
            if(!str) {
                str = `${i}`;
            }
            finalArr.push(str.trim());
        }

    }
    return finalArr.join(', ');
}

function testeUnitario1() {
    let expectedValue = "1, 2, fizz fizz, 4, buzz buzz, fizz, 7, 8, fizz, buzz, 11, fizz, fizz, 14, lucky, 16, 17, fizz, 19, buzz";
    let returnedValue = log(1, 20);
    if (returnedValue === expectedValue) {
        return "success<br><br>" + "O valor esperado para os testes com o valor inicial de 1 e final de 20 é: " + expectedValue + "<br><br>O resultado obtido foi de: " + returnedValue;
        console.log("success");
    } else {
        return "Error";
    }
}

function testeUnitario2(){
    let expectedValue = "1, 2, fizz fizz, 4, buzz buzz, fizz, 7, 8, fizz, buzz, 11, fizz, fizz, 14, lucky, 16, 17, fizz, 19, buzz, fizz, 22, fizz, fizz, buzz buzz, 26, fizz, 28, 29, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, buzz, 41, fizz, fizz, 44, lucky, 46, 47, fizz, 49, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, 61, 62, fizz fizz, 64, buzz buzz, fizz, 67, 68, fizz, buzz, 71, fizz, fizz, 74, lucky, 76, 77, fizz, 79, buzz, fizz, 82, fizz, fizz, buzz buzz, 86, fizz, 88, 89, lucky, 91, 92, fizz fizz, 94, buzz buzz, fizz, 97, 98, fizz, buzz, 101, fizz, fizz, 104, lucky, 106, 107, fizz, 109, buzz, fizz, 112, fizz, fizz, buzz buzz, 116, fizz, 118, 119, lucky, 121, 122, fizz fizz, 124, buzz buzz, fizz, 127, 128, fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, buzz, fizz, 142, fizz, fizz, buzz buzz, 146, fizz, 148, 149, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz, 161, fizz, fizz, 164, lucky, 166, 167, fizz, 169, buzz, fizz, 172, fizz, fizz, buzz buzz, 176, fizz, 178, 179, lucky, 181, 182, fizz fizz, 184, buzz buzz, fizz, 187, 188, fizz, buzz, 191, fizz, fizz, 194, lucky, 196, 197, fizz, 199, buzz, fizz, 202, fizz, fizz, buzz buzz, 206, fizz, 208, 209, lucky, 211, 212, fizz fizz, 214, buzz buzz, fizz, 217, 218, fizz, buzz, 221, fizz, fizz, 224, lucky, 226, 227, fizz, 229, fizz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz buzz, fizz, fizz fizz, fizz, fizz, lucky, 241, 242, fizz fizz, 244, buzz buzz, fizz, 247, 248, fizz, buzz buzz, buzz, fizz buzz, fizz buzz, buzz, lucky, buzz, buzz, fizz buzz, buzz, buzz, fizz, 262, fizz, fizz, buzz buzz, 266, fizz, 268, 269, lucky, 271, 272, fizz fizz, 274, buzz buzz, fizz, 277, 278, fizz, buzz, 281, fizz, fizz, 284, lucky, 286, 287, fizz, 289, buzz, fizz, 292, fizz, fizz, buzz buzz, 296, fizz, 298, 299, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz buzz, fizz buzz, fizz buzz, fizz fizz buzz, fizz buzz buzz, fizz buzz, fizz fizz buzz, fizz buzz, fizz buzz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, buzz, 401, fizz, fizz, 404, lucky, 406, 407, fizz, 409, buzz, fizz, 412, fizz, fizz, buzz buzz, 416, fizz, 418, 419, lucky, 421, 422, fizz fizz, 424, buzz buzz, fizz, 427, 428, fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, buzz, fizz, 442, fizz, fizz, buzz buzz, 446, fizz, 448, 449, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz, 461, fizz, fizz, 464, lucky, 466, 467, fizz, 469, buzz, fizz, 472, fizz, fizz, buzz buzz, 476, fizz, 478, 479, lucky, 481, 482, fizz fizz, 484, buzz buzz, fizz, 487, 488, fizz, buzz, 491, fizz, fizz, 494, lucky, 496, 497, fizz, 499, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz buzz, buzz, fizz buzz, fizz buzz, buzz, lucky, buzz, buzz, fizz buzz, buzz, fizz buzz buzz, fizz fizz buzz, fizz buzz, fizz buzz, fizz fizz buzz, fizz buzz buzz, fizz buzz, fizz fizz buzz, fizz buzz, fizz buzz, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz buzz, buzz, fizz buzz, fizz buzz, buzz, lucky, buzz, buzz, fizz buzz, buzz, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz buzz, buzz, fizz buzz, fizz buzz, buzz, lucky, buzz, buzz, fizz buzz, buzz, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, 601, 602, fizz fizz, 604, buzz buzz, fizz, 607, 608, fizz, buzz, 611, fizz, fizz, 614, lucky, 616, 617, fizz, 619, buzz, fizz, 622, fizz, fizz, buzz buzz, 626, fizz, 628, 629, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, buzz, 641, fizz, fizz, 644, lucky, 646, 647, fizz, 649, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, 661, 662, fizz fizz, 664, buzz buzz, fizz, 667, 668, fizz, buzz, 671, fizz, fizz, 674, lucky, 676, 677, fizz, 679, buzz, fizz, 682, fizz, fizz, buzz buzz, 686, fizz, 688, 689, lucky, 691, 692, fizz fizz, 694, buzz buzz, fizz, 697, 698, fizz, buzz, 701, fizz, fizz, 704, lucky, 706, 707, fizz, 709, buzz, fizz, 712, fizz, fizz, buzz buzz, 716, fizz, 718, 719, lucky, 721, 722, fizz fizz, 724, buzz buzz, fizz, 727, 728, fizz, fizz buzz, fizz, fizz fizz, fizz, fizz, lucky, fizz, fizz, fizz fizz, fizz, buzz, fizz, 742, fizz, fizz, buzz buzz, 746, fizz, 748, 749, lucky, buzz, buzz, fizz fizz buzz, buzz, buzz buzz, fizz buzz, buzz, buzz, fizz buzz, buzz, 761, fizz, fizz, 764, lucky, 766, 767, fizz, 769, buzz, fizz, 772, fizz, fizz, buzz buzz, 776, fizz, 778, 779, lucky, 781, 782, fizz fizz, 784, buzz buzz, fizz, 787, 788, fizz, buzz, 791, fizz, fizz, 794, lucky, 796, 797, fizz, 799, buzz, fizz, 802, fizz, fizz, buzz buzz, 806, fizz, 808, 809, lucky, 811, 812, fizz fizz, 814, buzz buzz, fizz, 817, 818, fizz, buzz, 821, fizz, fizz, 824, lucky, 826, 827, fizz, 829, fizz buzz, fizz fizz, fizz, fizz, fizz fizz, fizz buzz buzz, fizz, fizz fizz, fizz, fizz, lucky, 841, 842, fizz fizz, 844, buzz buzz, fizz, 847, 848, fizz, buzz buzz, buzz, fizz buzz, fizz buzz, buzz, lucky, buzz, buzz, fizz buzz, buzz, buzz, fizz, 862, fizz, fizz, buzz buzz, 866, fizz, 868, 869, lucky, 871, 872, fizz fizz, 874, buzz buzz, fizz, 877, 878, fizz, buzz, 881, fizz, fizz, 884, lucky, 886, 887, fizz, 889, buzz, fizz, 892, fizz, fizz, buzz buzz, 896, fizz, 898, 899, lucky, 901, 902, fizz fizz, 904, buzz buzz, fizz, 907, 908, fizz, buzz, 911, fizz, fizz, 914, lucky, 916, 917, fizz, 919, buzz, fizz, 922, fizz, fizz, buzz buzz, 926, fizz, 928, 929, lucky, fizz, fizz, fizz fizz, fizz, fizz buzz buzz, fizz fizz, fizz, fizz, fizz fizz, buzz, 941, fizz, fizz, 944, lucky, 946, 947, fizz, 949, buzz buzz, fizz buzz, buzz, fizz buzz, fizz buzz, buzz buzz, buzz, fizz buzz, buzz, buzz, lucky, 961, 962, fizz fizz, 964, buzz buzz, fizz, 967, 968, fizz, buzz, 971, fizz, fizz, 974, lucky, 976, 977, fizz, 979, buzz, fizz, 982, fizz, fizz, buzz buzz, 986, fizz, 988, 989, lucky, 991, 992, fizz fizz, 994, buzz buzz, fizz, 997, 998, fizz, buzz";
    let returnedValue = log(1,1000);
    if(returnedValue === expectedValue){
        return "success<br><br>" + "O valor esperado para os testes com o valor inicial de 1 e final de 1000 é: " + expectedValue + "<br><br>O resultado obtido foi de: " + returnedValue;
        console.log("success");
    }else{
        return "Error";
    }
}
